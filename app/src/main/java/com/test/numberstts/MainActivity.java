package com.test.numberstts;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements OnInitListener {

    private long number = 0;
    private static final int RESULT_CODE = 0;
    private TextToSpeech myTTS;

    TextView numberTextView;
    ImageView nextButton;
    ImageView previousButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Intent checkTTSIntent = new Intent();
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkTTSIntent, RESULT_CODE);

        numberTextView = findViewById(R.id.DisplayNumberTxtView);
        nextButton = findViewById(R.id.NextBtn);
        previousButton = findViewById(R.id.PreviousBtn);

        numberTextView.setText(String.valueOf(number));


        OnClickListener listener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.NextBtn:
                        number++;
                        break;

                    case R.id.PreviousBtn:
                        number--;
                        break;
                }

                numberTextView.setText(String.valueOf(number));
                speakWords(String.valueOf(number));


            }
        };

        numberTextView.setOnClickListener(listener);
        nextButton.setOnClickListener(listener);
        previousButton.setOnClickListener(listener);


    }


    private void speakWords(String speech) {
        myTTS.speak(speech, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == RESULT_CODE) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                myTTS = new TextToSpeech(this, this);
            } else {
                Intent installTTSIntent = new Intent();
                installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installTTSIntent);
            }
        }

    }

    public void onInit(int iniStatus) {
        if (iniStatus == TextToSpeech.SUCCESS) {
            myTTS.setLanguage(Locale.US);
        } else if (iniStatus == TextToSpeech.ERROR) {
            Toast.makeText(this, "TTS Failed", Toast.LENGTH_SHORT).show();
        }
    }

}
